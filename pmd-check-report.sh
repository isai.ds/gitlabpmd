#!/usr/bin/env

PMD_APEX_RULES_FILE_PATH="${SCA_BASE_PATH}/apex_ruleset.xml"

nameFile="pmd-base-report-$(date '+%d%m%Y%H%M%S').json"

pmd check --dir ${PMD_SALESFORCE_PATH} --rulesets ${PMD_APEX_RULES_FILE_PATH} --format json --report-file "${SCA_BASE_PATH}/${nameFile}" --no-fail-on-violation --cache "${SCA_BASE_PATH}/cache"

python3 "${PMD_CODE_QUALYTY_SCRIPT_PATH}/pmd-code-quality.py" --path "${SCA_BASE_PATH}/${nameFile}" --output-reports "gitlab-quality"