#!/usr/bin/env

PMD_APEX_RULES_FILE_PATH="${SCA_BASE_PATH}/apex_ruleset.xml"
PMD_DELTA_FILE_PATH="${SCA_BASE_PATH}/salesforce_diff_classes.txt"

APEX_QUALITY_REPORT_NAME="gl-code-quality-report.json"
nameFile="pmd-delta-$(date '+%d%m%Y%H%M%S').json" 

git diff-tree --name-only --no-commit-id -r ${CI_MERGE_REQUEST_DIFF_BASE_SHA}..${CI_COMMIT_SHA} ${PMD_SALESFORCE_PATH} > ${PMD_DELTA_FILE_PATH}

if [ -s ${PMD_DELTA_FILE_PATH} ]; then

    pmd check --file-list ${PMD_DELTA_FILE_PATH} --rulesets ${PMD_APEX_RULES_FILE_PATH} --format json --report-file "${SCA_BASE_PATH}/${nameFile}" --no-fail-on-violation --cache "${SCA_BASE_PATH}/cache"
        
    python3 "${PMD_CODE_QUALYTY_SCRIPT_PATH}/pmd-code-quality.py" --path "${SCA_BASE_PATH}/${nameFile}" --output-reports "gitlab-quality"

fi