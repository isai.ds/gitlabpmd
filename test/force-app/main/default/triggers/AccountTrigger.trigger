trigger AccountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) {    
    if(Trigger.isInsert && Trigger.isBefore){
        //AccountTriggerHandler.updateMediaAM(Trigger.new, Trigger.newMap, Trigger.oldMap);
        AccountTriggerHandler.beforeInsert(Trigger.new, Trigger.newMap, Trigger.oldMap);//added by Oanh        
    } else if( Trigger.isInsert && Trigger.isAfter){
        AccountTriggerHandler.afterInsert(Trigger.new, null);        
    } else if( Trigger.isUpdate && Trigger.isAfter){
        AccountTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);        
    } else if(Trigger.isBefore && Trigger.isUpdate){        
        AccountTriggerHandler.beforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);//added by Oanh        
    }     
}