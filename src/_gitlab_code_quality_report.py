import json
import os
import hashlib
import datetime

class GitlabCodeQualityReport:
    APEX_QUALITY_REPORT_NAME = 'gl-code-quality-report.json'    
    def __init__(self):
        self.md5Hashes = set()
        self.issues = []
    def getCodeQualityReport(self, path):
        self.getPMDFileResults(path)
        
        if  self.pmdResult['files']:
            self.parse()
            self.save()
    
    def parse(self):
        for file in self.pmdResult['files']:
            for violation in file['violations']:
                hash = self.getMD5Hash(file["filename"], violation["description"])
                issue = {
                    'type': 'issue',
                    'description': violation['description'],
                    'check_name': violation['rule'] + ' - ' + hash,
                    'fingerprint': hash,
                    'severity': GitlabCodeQualityReport.getSeverity(violation['priority']),
                    'location': {   
                        'path': file['filename'],
                        'lines': {
                            'begin': violation['beginline'],
                            'end': violation['endline']
                        }
                    }
                }
                self.issues.append(issue)
        return self.issues

    def getSeverity(severity):
        if severity == 5:
            return 'info'
        if severity == 4:
            return 'minor'
        if severity == 3:
            return 'major'
        if severity == 2:
            return 'critical'
        if severity == 1:
            return 'blocker'

    def getMD5Hash(self, filepath, message):
        m = hashlib.md5()        
        m.update(filepath.encode())
        m.update(message.encode())
        m.update(datetime.datetime.now().isoformat().encode())
        hash = m.hexdigest()
        while hash in self.md5Hashes:            
            m.update(hash.encode())
            hash = m.hexdigest()            
        self.md5Hashes.add(hash)
        return hash

    def getViolationsResume(self):
        resume = {
            'total': 0,
            'info': 0,
            'minor': 0,
            'major': 0,
            'critical': 0,
            'blocker': 0
        }

        for issue in self.issues:
            resume[issue['severity']] = resume[issue['severity']] + 1
            resume['total'] = resume['total'] + 1
        
        return resume

    def getPMDFileResults(self, path):
        f = open(path, 'r')    
        self.pmdResult = json.loads(f.read())
        f.close()
     
    def save(self):
        f = open(GitlabCodeQualityReport.APEX_QUALITY_REPORT_NAME, 'w')
        f.write(json.dumps(self.issues))
        f.close()