import gitlab
import os
import sys

class GitLab:
    CI_PIPELINE_URL = os.environ['CI_PIPELINE_URL']
    CI_SERVER_URL = os.environ['CI_SERVER_URL']
    CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
    CI_MERGE_REQUEST_IID = os.environ['CI_MERGE_REQUEST_IID']

    MESSAGE_WITHOUT_VIOLATIONS = '# Salesforce PMD Analysis Report\n' \
    'No violations found. Great work!'

    MESSAGE_WITH_VIOLATIONS = '# Salesforce PMD Analysis Report\n' \
    'Code quality violations found. Please check the [report]({ci_pipeline_url}) for details\n'

    VIOLATIONS_TABLE = '## Violations\n' \
    '| Blocker | Critical | Major | Minor | Info | Total |\n' \
    '| --- | --- | --- | --- | --- | --- |\n' \
    '{violations} \n'

    def __init__(self):
        if 'GITLAB_PIPELINE_TOKEN' not in os.environ:
            print('Missing Gitlab Acess Token, please add environment variable "export GITLAB_PIPELINE_TOKEN=<key>" and run this script again')
            sys.exit(1)
        
        self.gl = gitlab.Gitlab(url=GitLab.CI_SERVER_URL, private_token=os.environ['GITLAB_PIPELINE_TOKEN'])
        self.project = self.gl.projects.get(GitLab.CI_PROJECT_ID)
        self.mrs = self.project.mergerequests.get(GitLab.CI_MERGE_REQUEST_IID)

    def createDiscussion(self, violations):
        message = GitLab.MESSAGE_WITHOUT_VIOLATIONS
        if violations and violations['total'] > 0:
            message = GitLab.MESSAGE_WITH_VIOLATIONS.format(ci_pipeline_url = GitLab.CI_PIPELINE_URL + '/codequality_report')
            violationsTable = '| ' + str(violations['blocker']) + ' | ' + str(violations['critical']) + ' | ' + str(violations['major']) + ' | ' + str(violations['minor']) + ' | ' + str(violations['info']) + ' | ' + str(violations['total']) + ' |\n'
            message = message + GitLab.VIOLATIONS_TABLE.format(violations = violationsTable)

        self.mrs.notes.create({
            'body': message
        })