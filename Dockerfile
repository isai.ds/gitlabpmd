FROM ubuntu:focal as base

ENV DEBIAN_FRONTEND=noninteractive

# Install packages
RUN <<EOF
apt-get update
apt-get install -y curl unzip zip wget git default-jdk
apt-get install -y --no-install-recommends python3.9 python3.9-dev python3-pip build-essential 
rm -rf /var/lib/apt/lists/*
EOF

FROM eclipse-temurin:21 as pmd

ENV JAVA_HOME=/opt/java/openjdk

FROM  base
ARG PMD_VERSION

# Configuring java
COPY --from=pmd $JAVA_HOME $JAVA_HOME
ENV PATH="${JAVA_HOME}/bin:${PATH}"

# Download and unzip PMD
RUN <<EOF 
wget -q https://github.com/pmd/pmd/releases/download/pmd_releases/${PMD_VERSION}/pmd-dist-${PMD_VERSION}-bin.zip
unzip pmd-dist-${PMD_VERSION}-bin.zip
rm -r pmd-dist-${PMD_VERSION}-bin.zip
EOF

# Configuring python
RUN mkdir -p /usr/src/app
COPY requirements.txt /usr/src/app
RUN pip3 install --no-cache-dir -r /usr/src/app/requirements.txt
COPY . /usr/src/app

RUN <<EOF
chmod +x /usr/src/app/pmd-check-delta.sh
chmod +x /usr/src/app/pmd-check-report.sh
EOF

ENV PMD_CODE_QUALYTY_SCRIPT_PATH=/usr/src/app
ENV PATH=$PATH:/pmd-bin-${PMD_VERSION}/bin:/usr/src/app/