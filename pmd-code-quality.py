import argparse
import pathlib

from src import _gitlab_code_quality_report
from src import _gitlab

parser = argparse.ArgumentParser(description='GitLab PMD code quality check')
parser.add_argument('-p', '--path', help='Path to pmd check report', type= pathlib.Path, required=True)
parser.add_argument('-o', '--output-reports', choices=['gitlab-quality'], help='Reports to generate')

args = parser.parse_args()
codeQualityReport = _gitlab_code_quality_report.GitlabCodeQualityReport()
codeQualityReport.getCodeQualityReport(args.path)

gitlabAPI = _gitlab.GitLab()
gitlabAPI.createDiscussion(codeQualityReport.getViolationsResume())
 